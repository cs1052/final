package cs105;

import javax.swing.*;
import java.awt.*;

public class GUI {
    public static void main(String[] args) {
        Instructor hasanSozer = new Instructor("Hasan Sozer");
        Course cs102 = new Course("CS102", hasanSozer, 3);
        Course cs201 = new Course("CS201", hasanSozer, 1);
        hasanSozer.addCourse(cs102);
        hasanSozer.addCourse(cs201);

        JFrame frame = new JFrame("Project");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(650, 500);
        frame.setMinimumSize(new Dimension(650, 500));

        JTabbedPane tabbedPane = new JTabbedPane();

        JPanel instructorPanel = new InstructorPanel(hasanSozer);
        tabbedPane.add("Instructor", instructorPanel);

        frame.add(tabbedPane);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
