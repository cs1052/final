package cs105;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class InstructorPanel extends JPanel {

    private Instructor instructor;
    private JPanel buttonsPanel;
    private JPanel bottomPanel;

    public InstructorPanel(Instructor instructor) {
        this.instructor = instructor;
        this.setLayout(new GridLayout(2, 1));

        this.buttonsPanel = new JPanel();
        this.add(this.buttonsPanel);

        JButton listCoursesButton = new JButton("List Courses");
        JButton addCourseButton = new JButton("Add Course");
        this.buttonsPanel.add(listCoursesButton);
        this.buttonsPanel.add(addCourseButton);

        this.bottomPanel = new JPanel();
        this.add(this.bottomPanel);

        //Write your code here

        listCoursesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showListCoursesPanel();
            }
        });

        addCourseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showAddCoursePanel();
            }
        });
    }

    public void showListCoursesPanel() {
        ArrayList<Course> courses = instructor.getCourses();

        JPanel panel = new JPanel(new GridLayout(courses.size() + 1, 1));

        JLabel label1 = new JLabel("Name");
        label1.setForeground(Color.red);
        panel.add(label1);

        for (Course course : courses) {
            panel.add(new JLabel(course.getName()));
        }

        bottomPanel.removeAll();

        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.add(panel, BorderLayout.NORTH);
        bottomPanel.revalidate();

    }

    public void showAddCoursePanel() {
        JPanel panel = new JPanel();

        GridLayout gridLayout = new GridLayout(3, 2);
        gridLayout.setVgap(5);
        panel.setLayout(gridLayout);

        JTextField name = new JTextField();
        JFormattedTextField examCount = new JFormattedTextField(NumberFormat.getNumberInstance());
        JButton addCourseButton = new JButton("Add");

        panel.add(new JLabel("Name:"));
        panel.add(name);
        panel.add(new JLabel("# Exams:"));
        panel.add(examCount);
        panel.add(new JLabel(""));
        panel.add(addCourseButton);

        addCourseButton.addActionListener(e -> {
            if (name.getText() == null || name.getText().isEmpty()) {
                return;
            }
            if (examCount.getText().isEmpty()) {
                return;
            }
            Course c = new Course(name.getText(), instructor, Integer.parseInt(examCount.getText()));
            instructor.addCourse(c);
        });

        bottomPanel.removeAll();

        bottomPanel.add(panel, BorderLayout.NORTH);
        bottomPanel.revalidate();

    }
}
