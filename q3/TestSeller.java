package cs105;

public class TestSeller {
    public static void main(String[] args) {
        Seller s1 = new Seller("Jane");
        Seller s2 = new Seller("Jake", 3, 50);
        System.out.println(s1.getAmountEarned());
        System.out.println(s2.getAmountEarned());
        s1.sell(10, 100);
        System.out.println(s1.getAmountEarned());
        s2.sell(5, 50);
        System.out.println(s2.getAmountEarned());
    }
}
