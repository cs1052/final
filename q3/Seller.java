package cs105;

public class Seller extends Employee {
    private static double amountEarned;

    public Seller(String name) {
        super.setName(name);
        System.out.println("Seller 1 Arg Constructor");
    }

    public Seller(String name, int count, double price) {
        this(name);
        sell(count, price);
        System.out.println("Seller 3 Arg Constructor");
    }

    public double getAmountEarned() {
        if (amountEarned == 0)
            System.out.println("Earnings are 0 :(");
        return amountEarned;
    }

    public void sell(int count, double price) {
        amountEarned += count * price;
    }
}
