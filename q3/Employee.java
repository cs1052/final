package cs105;

public class Employee {
    private String name;

    public Employee() {
        System.out.println("Employee No Arg Constructor");
    }

    public Employee(String name) {
        setName(name);
        System.out.println("Employee 1 Arg Constructor");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        System.out.println("Employee setName");
    }
}
